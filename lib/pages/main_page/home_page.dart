import 'package:ahil_new_design/Utils/colors.dart';
import 'package:ahil_new_design/Utils/custom_widgets/custom_buttons/circle_button.dart';
import 'package:ahil_new_design/Utils/custom_widgets/user_profile_stuff/circle_color_status.dart';
import 'package:ahil_new_design/Utils/custom_widgets/neu_shapes/edit_text_shape.dart';
import 'package:ahil_new_design/Utils/custom_widgets/custom_widgets/edit_text.dart';
import 'package:ahil_new_design/Utils/custom_widgets/user_profile_stuff/profie_pictures.dart';
import 'package:ahil_new_design/Utils/custom_widgets/user_profile_stuff/recently_users.dart';
import 'package:ahil_new_design/Utils/custom_widgets/custom_widgets/text_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class HomePage extends StatefulWidget{

  const HomePage({Key? key}) : super(key: key);
  @override

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {

    Size _width = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: mainColor,
      body: SingleChildScrollView(
        child: SizedBox(
          width: _width.width,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                        top: 45,
                        right: 25),
                    child: Row(
                      children: [
                        const ProfileImg(
                            profilePic: "images/user.png"
                        ),
                        const SizedBox(
                          width: 5 ,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: const [
                                circleStatus(statusColor: redStatus),
                                SizedBox(
                                  width: 5,
                                ),
                                TextView(
                                    text: "سهیل امینی",
                                    color: black,
                                    size: 16,
                                    TxtDepth: 2),
                              ],
                            ),
                            const TextView(
                                text: "مشغول",
                                color: Colors.grey,
                                size: 14,
                                TxtDepth: 0),
                          ],
                        )
                      ],
                    )
                  ),
                  Container(
                      margin: const EdgeInsets.only(
                          top: 45,
                          left: 25),
                      child: Row(
                        children: [
                          CircleButton(
                              onPressed: (){},
                              icon: Icons.search,)
                        ],
                      )
                  )
                ],
              ),
              const SizedBox(
                height: 25,
              ),
              Container(
                width: _width.width,
                height: 120,
                margin: const EdgeInsets.only(
                  right: 20,
                ),
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Container(
                        margin: const EdgeInsets.only(
                            right: 15,
                            left: 15,
                            top: 10
                        ),
                        child: Column(
                          children: [
                            CircleButton(
                                onPressed: (){},
                                icon: Icons.add),
                            const SizedBox(
                              height: 10,
                            ),
                            const Text(
                              "جدید",
                              style: TextStyle(
                                  fontFamily: "sans",
                                  color: grey
                              ),
                            )
                          ],
                        )
                    ),
                    const RecentlyUsers(
                        profilePic: "images/user.png",
                        title: "اکبر",
                        statusColor: Colors.lightGreen,
                        count: "8",),
                    const RecentlyUsers(
                      profilePic: "images/user.png",
                      title: "اکبر",
                      statusColor: Colors.lightGreen,
                      count: "8",),
                    const RecentlyUsers(
                      profilePic: "images/user.png",
                      title: "اکبر",
                      statusColor: Colors.lightGreen,
                      count: "8",),
                    const RecentlyUsers(
                      profilePic: "images/user.png",
                      title: "اکبر",
                      statusColor: Colors.lightGreen,
                      count: "8",),
                    const RecentlyUsers(
                      profilePic: "images/user.png",
                      title: "اکبر",
                      statusColor: Colors.lightGreen,
                      count: "8",),
                  ],
                ),
              )
            ],
          ),
        )
      ),
    );
  }
}
