
import 'package:ahil_new_design/Utils/colors.dart';
import 'package:ahil_new_design/Utils/custom_widgets/custom_buttons/button_left_side.dart';
import 'package:ahil_new_design/Utils/custom_widgets/custom_buttons/button_right_side.dart';
import 'package:ahil_new_design/Utils/custom_widgets/neu_shapes/edit_text_shape.dart';
import 'package:ahil_new_design/Utils/custom_widgets/custom_widgets/edit_text.dart';
import 'package:ahil_new_design/Utils/custom_widgets/neu_shapes/neu_circle.dart';
import 'package:ahil_new_design/Utils/page_rout.dart';
import 'package:ahil_new_design/pages/main_page/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  @override
  Widget build(BuildContext context) {

    Size _width = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: mainColor,
      body: SingleChildScrollView(
          child: SizedBox(
            width: _width.width,
            child: Column(
              children: [
                //each row, have one item!
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 35, left: 25),
                      child: NeuCircle(
                        width: 200,
                        height: 200,
                        color: mainColor,
                        radius: 200,
                        depth: 8,
                        child: Image.asset(
                            "images/logo.png",
                        ),
                      ) ,
                    )
                  ],
                ),
                Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 51),
                        child: Text(
                            "به آهیل خوش آمدی",
                            style: TextStyle(
                                color: const Color(0xff727477).withOpacity(0.61),
                                fontFamily: "sans",
                                fontSize: 20
                            )
                        )
                    ),
                  ],
                ),
                Row(
                  textDirection: TextDirection.rtl,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          right: 50,
                          top: 5),
                      child: const Text(
                          "ورود به حساب",
                      style: TextStyle(
                        color: white,
                        fontFamily: "sans",
                        fontSize: 20
                      )
                      )
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: (_width.width) - 100,
                      height: 50,
                      margin: const EdgeInsets.only(
                        top: 15,
                      ),
                      child: EditTextShape(
                        child: EditText(
                            hint: "نام کاربری",
                            hintColor: const Color(0xff828282).withOpacity(0.56),
                            TxtColor: const Color(0xff828282).withOpacity(0.56),),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: (_width.width) - 100,
                      height: 50,
                      margin: const EdgeInsets.only(
                        top: 15,
                      ),
                      child: EditTextShape(
                        child: EditText(
                            hint: "گذرواژه",
                            hintColor: const Color(0xff828282).withOpacity(0.56),
                            TxtColor: const Color(0xff828282).withOpacity(0.56),
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        margin: const EdgeInsets.only(
                            right: 48
                        ),
                        child: TextButton(
                          onPressed: () {},
                          child: Text(
                            "گذرواژه خودت رو فراموش کردی؟",
                            style: TextStyle(
                                fontSize: 12,
                                fontFamily: "sans",
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline,
                                color: const Color(0xff828282).withOpacity(0.78),
                            ),
                          ),
                        )
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          top: 50
                      ),
                      child: BtnLeftSide(
                        title: "ورود",
                        icon: Icons.arrow_forward,
                        onPressed: () {
                          Navigator.push(
                            context,
                            CustomPageRout(
                                child: const HomePage(),
                                direction: AxisDirection.left
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(
                          top: 25
                      ),
                      child: BtnRightSide(
                          title: "ساخت حساب",
                          icon: Icons.person_add_alt_1_rounded,
                          width: 110,
                          onPressed: (){}
                      )
                    ),
                  ],
                ),
              ],
            ),
          )
      ),
    );
  }
}