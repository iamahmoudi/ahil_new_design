
import 'package:ahil_new_design/Utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

//circle shape

class NeuCircle extends StatelessWidget{

  final double width;
  final double height;
  final double radius;
  final double depth;
  final Color color;
  final Widget child;

  const NeuCircle({
    Key? key,
    required this.width,
    required this.height,
    required this.color,
    required this.radius,
    required this.depth,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: height,
        width: width,
        child: Neumorphic(
            style: NeumorphicStyle(
              shape: NeumorphicShape.flat,
              boxShape: NeumorphicBoxShape.roundRect
                (BorderRadius.circular(radius)),
              depth: depth,
              color: color,
              lightSource: LightSource.topLeft,
              intensity: 0.5,
              surfaceIntensity: 1,
              //shadowLightColorEmboss: white,
              //shadowLightColor: black
            ),
            child: child
        )
    );
  }
}