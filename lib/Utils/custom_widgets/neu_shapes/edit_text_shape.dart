
import 'package:ahil_new_design/Utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

// base of edit texts

class EditTextShape extends StatelessWidget{

  final Widget child;

  const EditTextShape({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        child: Neumorphic(
          style: NeumorphicStyle(
            shape: NeumorphicShape.concave,
            boxShape: NeumorphicBoxShape.roundRect
              (BorderRadius.circular(200)),
            depth: -2,
            color: mainColor,
            lightSource: LightSource.topLeft,
            intensity: 0.9,
            surfaceIntensity: 10,
            shadowLightColorEmboss: white,
          ),
          child: child
        )
    );
  }
}