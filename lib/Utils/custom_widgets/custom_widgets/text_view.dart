import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

// Neumorphic text view

class TextView extends StatelessWidget{

  final String text;
  final Color color;
  final double size;
  final double TxtDepth;

  const TextView({
    Key? key,
    required this.text,
    required this.color,
    required this.size,
    required this.TxtDepth
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
  return NeumorphicText(
    text,
    style: NeumorphicStyle(
      depth: 4,
      color: color,
    ),
    textStyle: NeumorphicTextStyle(
        fontSize: size,
        fontFamily: "sans",
        fontWeight: FontWeight.bold
    ),
  );
  }
}