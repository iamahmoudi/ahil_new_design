import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// class for edit texts

class EditText extends StatelessWidget{

  final String hint;
  final Color hintColor;
  final Color TxtColor;

  const EditText({
    Key? key,
    required this.hint,
    required this.hintColor,
    required this.TxtColor
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: hint,
        hintStyle: TextStyle(
            color: hintColor,
            fontSize: 14
        ),
        border: const OutlineInputBorder(
            borderSide: BorderSide.none),
      ),
      style: TextStyle(
          fontFamily: "sans",
          color: TxtColor
      ),
    );
  }
}