
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

import '../../colors.dart';
import '../../colors.dart';

class circleStatus extends StatelessWidget{

  final Color statusColor;

  const circleStatus({
    Key? key,
    required this.statusColor
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 12,
        width: 12,
        decoration: BoxDecoration(
          border: Border.all(
            color: white,
            width: 1.5
          ),
          borderRadius: BorderRadius.circular(50)
        ),
        child: GestureDetector(
          onTap: (){},
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Container(
              color: statusColor,
            ),
          ),
        )
    );
  }
  }
