
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import '../../colors.dart';

//shape of unread counts

class unreadCounts extends StatelessWidget{

  final String counts;

  const unreadCounts({
    Key? key,
    required this.counts
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 17,
        width: 17,
        child: GestureDetector(
          onTap: (){},
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Container(
              alignment: Alignment.center,
              color: orange,
              child:  Text(
                counts,
                style: const TextStyle(
                color: white
              ),
            ),
            ),
          ),
        )
    );
  }
}
