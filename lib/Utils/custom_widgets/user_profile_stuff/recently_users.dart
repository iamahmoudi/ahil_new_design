import 'package:ahil_new_design/Utils/custom_widgets/user_profile_stuff/circle_color_status.dart';
import 'package:ahil_new_design/Utils/custom_widgets/user_profile_stuff/unread_counts.dart';
import 'package:flutter/material.dart';

import '../../colors.dart';
import 'circle_color_status.dart';

//just profile picture and unread counts

class RecentlyUsers extends StatelessWidget{

  final String profilePic;
  final VoidCallback? onPressed;
  final String title;
  final String count;
  final Color statusColor;

  const RecentlyUsers({
    Key? key,
    required this.profilePic,
    this.onPressed,
    required this.title,
    required this.statusColor,
    required this.count
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        right: 15,
        left: 15
      ),
        child: Column(
          children: [
            Container(
                height: 70,
                width: 70,
                child: Stack(
                  children: [
                    GestureDetector(
                      onTap: onPressed,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Image.asset(
                          profilePic,
                          height: 150,
                          width: 150,
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                        top: 55,
                        right: 4
                      ),
                      child: circleStatus(
                          statusColor: statusColor),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                        right: 55
                      ),
                      child: unreadCounts(
                          counts: count
                      )
                    )
                  ],
                )
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              title,
              style: const TextStyle(
                  fontFamily: "sans",
                  color: grey
              ),
            )
          ],
        )
    );
  }
}