import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import '../../colors.dart';

// profile picture

class ProfileImg extends StatelessWidget{

  final String profilePic;
  final VoidCallback? onPressed;

  const ProfileImg({
    Key? key,
    required this.profilePic,
    this.onPressed
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 70,
        width: 70,
        margin: const EdgeInsets.only(
            right: 5,
            left: 5
        ),
          child: GestureDetector(
            onTap: onPressed,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Image.asset(
                profilePic,
                height: 150,
                width: 150,
              ),
            ),
          )
    );
  }
}