import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

import '../../colors.dart';

// button with icon on right

class BtnRightSide extends StatelessWidget{

  final String title;
  final IconData? icon;
  final VoidCallback? onPressed;
  final double width;

  const BtnRightSide({
    Key? key,
    required this.title,
    required this.icon,
    required this.onPressed,
    required this.width
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NeumorphicButton(
      style: NeumorphicStyle(
        shape: NeumorphicShape.concave,
        boxShape: NeumorphicBoxShape.roundRect
          (BorderRadius.circular(200)),
        depth: 8,
        color: mainColor,
          lightSource: LightSource.topLeft,
          intensity: 0.6,
          surfaceIntensity: 0.2,
          shadowLightColor: white
      ),
      child: Container(
        width: width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                child: Icon(
                  icon,
                  color: const Color(0xff828282).withOpacity(0.43),
                ),
              ),
            ),
            const Spacer(),
            Expanded(
              flex: 9,
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  title,
                  style: TextStyle(
                      fontFamily: "sans",
                      fontSize: 12,
                      color: const Color(0xff575F6B).withOpacity(0.60)
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      onPressed: onPressed,
    );
  }
}