import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

import '../../colors.dart';

// button with icon on left

class BtnLeftSide extends StatelessWidget{

  final String title;
  final IconData? icon;
  final VoidCallback? onPressed;

  const BtnLeftSide({
    Key? key,
    required this.title,
    required this.icon,
    this.onPressed
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NeumorphicButton(
      style: NeumorphicStyle(
        shape: NeumorphicShape.concave,
        boxShape: NeumorphicBoxShape.roundRect
          (BorderRadius.circular(200)),
        depth: 8,
        color: mainColor,
        shadowLightColorEmboss: white,
        lightSource: LightSource.topLeft,
        shadowLightColor: white,
        surfaceIntensity: 0.2,
        intensity: 0.6
      ),
      child: Container(
        width: 150,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 9,
              child: Container(
                alignment: Alignment.center,
                child: Text(
                  title,
                  style: const TextStyle(
                      fontFamily: "sans",
                      fontSize: 19,
                      color: Color(0xff707070)
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Icon(
                icon,
                color: const Color(0xff707070),
              ),
            )
          ],
        ),
      ),
      onPressed: onPressed,
    );
  }
}