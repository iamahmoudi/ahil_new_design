import 'package:ahil_new_design/Utils/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';

// circle button with icon inside

class CircleButton extends StatelessWidget{

  final VoidCallback onPressed;
  final IconData icon;
  //final Color color;

  const CircleButton({
    Key? key,
    required this.onPressed,
    required this.icon,
    //required this.color
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 55,
      height: 55,
      alignment: Alignment.center,
      child: NeumorphicButton(
        style: NeumorphicStyle(
          shape: NeumorphicShape.concave,
          boxShape: NeumorphicBoxShape.roundRect
            (BorderRadius.circular(300)),
          depth: 5,
          lightSource: LightSource.topLeft,
          color: mainColor,
        ),
        child: NeumorphicIcon(
        icon,
        size: 40,
          style: NeumorphicStyle(
            color: grey.withOpacity(0.5)
          ),
      ),
        onPressed: onPressed,
      ),
    );
  }
}