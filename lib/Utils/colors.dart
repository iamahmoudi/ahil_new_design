import 'package:flutter/material.dart';

const white = Color(0xFFFFFFFF);
const mainColor = Color(0xffEDEEF2);
const orange = Color(0xffff9900);
const black = Color(0xff000000);
const grey = Color(0xff707070);
const redStatus = Color(0xffff0023);